/**
 * @Title: TestQuickSort.java
 * @Package com.adamjwh.test
 * @Description: 
 * @author adamjwh
 * @date 2018年6月25日
 * @version V1.0
 */
package com.adamjwh.test;

/**
 * @ClassName: TestQuickSort
 * @Description: 快排
 * @author adamjwh
 * @date 2018年6月25日
 *
 */
public class TestQuickSort {
	
	public static void InsertSort(int[] arr) {
		int j, current;
		
		for(int i=1; i<arr.length; i++) {
			j = i-1;
			current = arr[i];
			while(j>=0 && arr[j]>current) {
				arr[j+1] = arr[j];
				j--;
			}
			arr[j+1] = current;
		}
	}
	

	public static int partition(int[] arr, int low, int high) {
		int key = arr[low];
		
		while(low < high) {
			while(low<high && arr[high]>=key) {
				high--;
			}
			arr[low] = arr[high];
			while(low<high && arr[low]<=key) {
				low++;
			}
			arr[high] = arr[low];
		}
		arr[low] = key;
		
		return low;
	}
	
	public static void sort(int[] arr, int low, int high) {
		if(low>=high) {
			return;
		}
		
		if(high-low+1 < 10) {
			InsertSort(arr);
		}
		
		int index = partition(arr, low, high);
		sort(arr, low, index-1);
		sort(arr, index+1, high);
	}
	
	public static void main(String[] args) {
		int[] arr = new int[] {49, 38, 65, 97, 76, 13, 27};
		
		sort(arr, 0, arr.length-1);	//快排
		
		for (int i : arr) {	//输出
			System.out.print(i + " ");
		}
	}
	
}
