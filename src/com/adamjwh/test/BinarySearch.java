/**
 * @Title: BinarySearch.java
 * @Package com.adamjwh.test
 * @Description: 
 * @author adamjwh
 * @date 2018年7月19日
 * @version V1.0
 */
package com.adamjwh.test;

/**
 * @ClassName: BinarySearch
 * @Description: 二分查找
 * @author adamjwh
 * @date 2018年7月19日
 *
 */
public class BinarySearch {
	
	public int binarySearch(int[] arr, int x) {
		if(arr==null || arr.length<=0) {
			return -1;
		}
		
		int low = 0;
		int high = arr.length - 1;
		int middle = (low + high) >> 1;
		
		while(low <= high) {
			if(arr[middle] < x) {
				low = middle + 1;
			} else if(arr[middle] > x) {
				high = middle - 1;
			} else {
				return middle;
			}
		}
		
		return -1;
	}

}
