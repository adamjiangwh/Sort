/**
 * @Title: InsertSort.java
 * @Package com.adamjwh.sort.quicksort
 * @Description: 
 * @author adamjwh
 * @date 2018年6月25日
 * @version V1.0
 */
package com.adamjwh.sort.quicksort;

/**
 * @ClassName: InsertSort
 * @Description: 插入排序
 * @author adamjwh
 * @date 2018年6月25日
 *
 */
public class InsertSort {

	public static void insertSort(int[] arr) {
		int j, current;
		
		//假定第一个元素被放到了正确的位置上
		for(int i=1; i<arr.length; i++) {
			j = i-1;
			current = arr[i];
			while(j>=0 && arr[j]>current) {
				arr[j+1] = arr[j];
				j --;
			}
			arr[j+1] = current;
		}
	}
	
}
