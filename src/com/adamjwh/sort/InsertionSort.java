package com.adamjwh.sort;

import java.util.Arrays;

/**
 * 插入排序
 * 
 * @author adamjwh
 *
 * 算法描述：
 * 从第一个元素开始，该元素可以认为已经被排序；
 * 取出下一个元素，在已经排序的元素序列中从后向前扫描；
 * 如果该元素（已排序）大于新元素，将该元素移到下一位置；
 * 重复步骤3，直到找到已排序的元素小于或者等于新元素的位置；
 * 将新元素插入到该位置后；
 * 重复步骤2~5。
 * 
 * 最佳情况：T(n) = O(n)   最坏情况：T(n) = O(n2)   平均情况：T(n) = O(n2)
 */
public class InsertionSort {

	public void insertionSort(int[] array) {
		int length = array.length;
		int preIndex, current;
		
		for(int i=1; i<length; i++) {
			preIndex = i-1;
			current = array[i];
			while(preIndex >= 0 && array[preIndex] > current) {
				array[preIndex+1] = array[preIndex];
				preIndex--;
			}
			array[preIndex+1] = current;
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {10,15,25,37,21,13,9,10,15,2};
		InsertionSort insertionSort = new InsertionSort();
		insertionSort.insertionSort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
