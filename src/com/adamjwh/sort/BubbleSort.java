package com.adamjwh.sort;

import java.util.Arrays;

/**
 * 冒泡排序
 * 
 * @author adamjwh
 * 
 * 算法描述：
 * 比较相邻的元素。如果第一个比第二个大，就交换它们两个；
 * 对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对，这样在最后的元素应该会是最大的数；
 * 针对所有的元素重复以上的步骤，除了最后一个；
 * 重复步骤1~3，直到排序完成。
 * 
 * 最佳情况：T(n) = O(n)   最差情况：T(n) = O(n2)   平均情况：T(n) = O(n2)
 */
public class BubbleSort {

	public void bubbleSort(int[] arr) {
		int length = arr.length;
		int temp;
		
		for(int i=0; i<length; i++) {
			for(int j=0; j<length-i-1; j++) {
				if(arr[j] > arr[j+1]) {
					temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[] arr = {10,15,25,37,21,13,9,10,15,2};
		BubbleSort bubbleSort = new BubbleSort();
		bubbleSort.bubbleSort(arr);
		System.out.println(Arrays.toString(arr));
	}

}
